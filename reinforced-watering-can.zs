recipes.remove(<ExtraUtilities:watering_can:3>,
[
[<ExtraUtilities:bedrockiumIngot>, <ExtraUtilities:mini-soul>, null],
[<ExtraUtilities:bedrockiumIngot>, <minecraft:bowl>, <ExtraUtilities:bedrockiumIngot>],
[null, <ExtraUtilities:bedrockiumIngot>, null]
]
);
recipes.addShaped(<ExtraUtilities:watering_can:3>,
[[<ExtraUtilities:bedrockiumIngot>, <ForbiddenMagic:StarBlock>, null],
 [<ExtraUtilities:bedrockiumIngot>, <minecraft:bowl>, <ExtraUtilities:bedrockiumIngot>],
 [null, <ExtraUtilities:bedrockiumIngot>, null]]);
